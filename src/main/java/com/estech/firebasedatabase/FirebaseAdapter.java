package com.estech.firebasedatabase;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class FirebaseAdapter extends RecyclerView.Adapter<FirebaseAdapter.ToDoItemViewHolder> {

    private FirebaseFirestore db;
    private Context mContext;
    private List<ToDoItem> mList;

    FirebaseAdapter(Context context, FirebaseFirestore db) {
        mList = new ArrayList<>();
        this.db = db;
        mContext = context;
    }

    @NonNull
    @Override
    public ToDoItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ToDoItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ToDoItemViewHolder holder, int position) {
        final ToDoItem toDoItem = mList.get(position);

        holder.txtItem.setText(toDoItem.getName());
        holder.txtUser.setText(toDoItem.getUsername());

        if (toDoItem.isCompleted()) {
            holder.imgDone.setVisibility(View.VISIBLE);
        } else {
            holder.imgDone.setVisibility(View.INVISIBLE);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ItemActivity.class);
                intent.putExtra(Constant.INTENT_TODO, toDoItem);
                mContext.startActivity(intent);
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ToDoItem item = mList.get(holder.getAdapterPosition());
                item.setCompleted(!item.isCompleted());
                db.collection("TodoList").document(item.getId()).update("completed", item.isCompleted());
                return true;
            }
        });
    }

    void updateAdapter(List<ToDoItem> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class ToDoItemViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView txtItem, txtUser;
        ImageView imgDone;

        ToDoItemViewHolder(View view) {
            super(view);
            mView = view;
            txtItem = view.findViewById(R.id.txtItem);
            txtUser = view.findViewById(R.id.txtUser);
            imgDone = view.findViewById(R.id.imgDone);
        }
    }
}
package com.estech.firebasedatabase;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Qastusoft
 * Created by Sergio on 07/06/2017.
 */

public class ItemActivity extends AppCompatActivity {

    private String item_id;
    private TextView text_user, text_title, text_msg, text_complete, text_date;
    private Button marcar, eliminar;
    private ToDoItem toDoItem;
    private String username;
    private FirebaseFirestore db;
    private CollectionReference collection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_detail);

        SharedPreferences prefs = getApplication().getSharedPreferences(Constant.PREF_TODO, MODE_PRIVATE);
        username = prefs.getString(Constant.PREF_USERNAME, null);

        text_user = findViewById(R.id.text_user);
        text_title = findViewById(R.id.text_title);
        text_msg = findViewById(R.id.text_message);
        text_complete = findViewById(R.id.text_completed);

        marcar = findViewById(R.id.butt_complete);
        eliminar = findViewById(R.id.butt_delete);

        db = FirebaseFirestore.getInstance();
        collection = db.collection("TodoList");

        Bundle bundle = getIntent().getExtras();
        int id = bundle.getInt(Constant.INTENT_TODO);
        toDoItem = bundle.getParcelable(Constant.INTENT_TODO);
        text_user.setText(toDoItem.getUsername());
        text_title.setText(toDoItem.getName());
        text_msg.setText(toDoItem.getMessage());
        setTextCompleted(toDoItem.isCompleted());
        marcarButton();
        deleteButton();
    }

    private void setTextCompleted(boolean isCompleted) {
        if (isCompleted) {
            text_complete.setText(R.string.completed);
            marcar.setText(R.string.button_no_complete);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                text_complete.setTextColor(ContextCompat.getColor(ItemActivity.this, android.R.color.holo_green_dark));
            } else {
                text_complete.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            }
        } else {
            text_complete.setText(R.string.no_completed);
            marcar.setText(R.string.button_complete);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                text_complete.setTextColor(ContextCompat.getColor(ItemActivity.this, android.R.color.holo_red_dark));
            } else {
                text_complete.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
        }

    }

    private void deleteButton() {
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collection.document(toDoItem.getId()).delete();
                finish();
            }
        });
    }

    private void marcarButton() {
        marcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDoItem.setCompleted(!toDoItem.isCompleted());
                db.collection("TodoList").document(toDoItem.getId()).update("completed", toDoItem.isCompleted());
                setTextCompleted(toDoItem.isCompleted());
            }
        });
    }
}

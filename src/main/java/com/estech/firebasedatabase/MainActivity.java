package com.estech.firebasedatabase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * To Do
 * Created by Sergio on 17/08/2016.
 * Qastusoft
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ToDoApp - Main";

    private FirebaseFirestore db;

    private FirebaseAdapter adapter;
    private RecyclerView recyclerview;
    private List<ToDoItem> todoList;
    private String username;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        db = FirebaseFirestore.getInstance();
        username = setupUsername();
        setTitle(username);

        recyclerview = findViewById(R.id.recycler_view_items);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FirebaseAdapter(MainActivity.this, db);
        recyclerview.setAdapter(adapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(MainActivity.this, ToDoActivity.class);
                newIntent.putExtra(Constant.PREF_USERNAME, username);
                startActivity(newIntent);
            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        ToDoItem item = todoList.get(position);
                        db.collection("TodoList").document(item.getId()).delete();

                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerview);

        // loadData();

        CollectionReference collection = db.collection("TodoList");
        collection.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                todoList = new ArrayList<>();
                for (DocumentSnapshot doc : queryDocumentSnapshots) {
                    String name = doc.getString("name");
                    String message = doc.getString("message");
                    boolean completed = doc.getBoolean("completed");
                    String username = doc.getString("username");
                    ToDoItem toDoItem = new ToDoItem(name, message, username, completed);
                    toDoItem.setId(doc.getId());
                    todoList.add(toDoItem);
                }
                adapter.updateAdapter(todoList);
            }
        });
    }

    private void loadData() {
        db.collection("TodoList").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot doc : task.getResult()) {
                        String name = doc.getString("name");
                        String message = doc.getString("message");
                        boolean completed = doc.getBoolean("completed");
                        String username = doc.getString("username");
                        ToDoItem toDoItem = new ToDoItem(name, message, username, completed);
                        todoList.add(toDoItem);
                    }
                }
            }
        });
    }

    private String setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences(Constant.PREF_TODO, MODE_PRIVATE);
        String username = prefs.getString(Constant.PREF_USERNAME, null);
        if (username == null) {
            Random r = new Random();
            username = "AndroidUser" + r.nextInt(100000);
            prefs.edit().putString(Constant.PREF_USERNAME, username).apply();
        }
        return username;
    }

}

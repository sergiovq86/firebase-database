package com.estech.firebasedatabase;

/**
 * Qastusoft
 * Created by Sergio on 07/06/2017.
 */

public class Constant {
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_NAME = "name";
    public static final String ITEM_MESSAGE = "message";
    public static final String ITEM_COMPLETED = "completed";
    public static final String ITEM_DATE = "date";
    public static final String ITEM_USER = "username";
    public static final String FIREBASE_CHILD = "ToDoApp";
    public static final String FIREBASE_TOKENS_CHILD = "ToDoTokens";

    public static final String INTENT_TODO = "todointent";

    //FCM
    public static final String TAG = "MyFirebaseIIDService";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String TOKEN = "token";
    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    public static final String FCM_KEY = "AIzaSyBiwlEB5GB0GAGqP3xlgAs5RfY1jknh4gc";

    //PARAMS FCM
    public static final String FCM_BODY = "body";
    public static final String FCM_TITLE = "title";
    public static final String FCM_MESSAGE = "message";
    //public static final String FCM_ID = "id";
    public static final String FCM_NOTIFICATION = "notification";
    public static final String FCM_DATA = "data";
    public static final String FCM_REG_IDS = "registration_ids";
    public static final String FCM_AUTH = "Authorization";
    public static final String FCM_CONTENT = "Content-Type";

    //SharedPreferences
    public static final String PREF_TODO = "ToDoPrefs" ;
    public static final String PREF_USERNAME = "username" ;
}
